#ifndef KINDEXEDMAP_H
#define KINDEXEDMAP_H

#include <stdlib.h>

#include <ksortedlist.h>
#include <khashmap.h>

typedef struct kindexedmap_t {
	ksortedlist_t index;
	khashmap_t *map;
} kindexedmap_t;

/*
 * I had one use of khashmap and klist together to maintain an sortable order to the keys
 * Only one instance but seemed useful
 *
 * probably bad naming with indexed, indexed meaning there is a sortable index that can be maintained.
 */

void kindexedmap_init_allocs(kindexedmap_t *imap, klist_comparison_f index_compare, size_t map_table_size);


void kindexedmap_release_frees(kindexedmap_t *imap);
void *kindexedmap_put_allocs(kindexedmap_t *imap, khashmap_key_t *key, void *value);
void *kindexedmap_get(kindexedmap_t *imap, khashmap_key_t *key);
void *kindexedmap_remove_frees(kindexedmap_t *imap, khashmap_key_t *key);
#endif
