/**
 * @file khashmap.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Small quick hashmap implemention for char* keys
 * @see https://klib.gitlab.io/libktypes/
 */
#ifndef KHASHMAP_H
#define KHASHMAP_H

#include <stdint.h>
#include <stddef.h>
#include <klist.h>




#define KHASHMAP_FOR_EACH_VALUE(map, entry)      for (size_t __KHMFEV_i = 0; __KHMFEV_i < (map)->table_size; __KHMFEV_i++) \
		for (entry = (khashmap_entry_t*)(map)->buckets[__KHMFEV_i].values.head; entry != NULL; entry = (khashmap_entry_t*)entry->node.next)
typedef struct khashmap_key_t {
	size_t len;
	uint8_t *data;
} khashmap_key_t;


/**
 * @brief A key and value
 *
 */
typedef struct khashmap_entry_t {
	//entry will never be added to a different list.
	klist_node_t node;//in this case, value will point back to the node itself.
	khashmap_key_t key;

	uint32_t key_hash;
	//we'd have to store a ptr to hash method if we used void* and its too much for me now
	void *value;
} khashmap_entry_t;

/**
 * @brief A list of entries all belonging to the same index
 *
 */
typedef struct khashmap_bucket_t {
	size_t index;
	klist_t values;
} khashmap_bucket_t;

/**
 * @brief A table of buckets each with list of key+value pairs
 *
 */
typedef struct khashmap_t {
	size_t total_entries;
	size_t table_size;
	khashmap_bucket_t buckets[0];
} khashmap_t;


/**
 * @brief Allocates and initializes a new hashmap with specified table size
 *
 * @param table_size p_table_size: Has direct performance impact more cpu for less (initial) memory, or less cpu with memory as a cost
 * @return khashmap_t*
 */
khashmap_t *khashmap_new_allocs(size_t table_size);

/**
 * @brief Given hashmap, return value using key as hash
 *
 * @param hashmap p_hashmap:...
 * @param key p_key:...
 * @return void* | A ptr to the value stored with the key
 */
void *khashmap_get(khashmap_t *hashmap, khashmap_key_t *key);

/**
 * @brief Given a hashmap, upsert the value as a newly allocated entry with key
 *
 * @param hashmap p_hashmap:...
 * @param key p_key:...
 * @param value p_value:...
 * @return void* |The original value if present
 */
void *khashmap_put_allocs(khashmap_t *hashmap, khashmap_key_t *key, void *value);

void *khashmap_remove_frees(khashmap_t *hashmap, khashmap_key_t *key);

/**
 * @brief Iterates each table bucket releases entries,nodes and itself
 *
 * @param hashmap p_hashmap:...
 */
void khashmap_release_frees(khashmap_t *hashmap);

uint32_t khashmap_key_hash(khashmap_key_t *key);
int khashmap_key_compare(khashmap_key_t *left, khashmap_key_t *key);
#endif
