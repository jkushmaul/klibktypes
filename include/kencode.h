/**
 * @file kencode.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Small quick hex encoding and auto allocating sprintf
 * @see https://klib.gitlab.io/libktypes/
 */
#ifndef KENCODE_H
#define KENCODE_H
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>

enum kencode_error { KENCODE_NO_ERROR = 0, KENCODE_BAD_LENGTH, KENCODE_BAD_CHAR };

/**
 * @brief Given a byte, adds 2-byte hex encoding to dec_raw
 *
 * @param enc_char p_enc_char:...
 * @param dec_raw p_dec_raw:must be at minimum size of 2
 * @return int
 */
int kencode_hex_value(char enc_char, uint8_t *dec_raw);

/**
 * @brief Given src of srclen, and dst of dstlen (srclen *2) will encoded lowercase hex
 *
 * @param src p_src:...
 * @param srclen p_srclen:...
 * @param dst p_dst:...
 * @param dstlen p_dstlen:Should be srclen * 2
 * @return int
 */
int kencode_hex_encode(uint8_t *src, size_t srclen, char *dst, size_t dstlen);

/**
 * @brief Given src hex string, decodes to dst bytes
 *
 * @param src p_src:...
 * @param srclen p_srclen:...
 * @param dst p_dst:...
 * @param dstlen p_dstlen: Should be srclen/2
 * @return int | 0 on success, -1 on error
 */
int kencode_hex_decode(const char *src, size_t srclen, uint8_t *dst, size_t dstlen);


#endif
