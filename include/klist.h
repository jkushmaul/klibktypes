/**
 * @file klist.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief A DL list in C
 * @see https://klib.gitlab.io/libktypes/
 */
#ifndef LIST_H
#define LIST_H

#ifndef __cplusplus
#include <stdatomic.h>
#else
#ifndef ATOMIC_INT_LOCK_FREE
//this isn't a C++ library so I'm not adding it unless I find a safe way to do it
#error C++ users, Please include <atomic> and "using namespace std;" before including this file.
#endif
#endif

/**
 * @brief supposed to make it quick+std to foreach a list
 *
 */
#define KLIST_FOR_EACH_NODE(node, list) for (klist_node_t* node = (list)->head; node != 0; node = node->next)

/**
 * @brief Single node in a list, next,prev,value
 * Caller must free if removed from list
 *
 */
#define  KLIST_NODE_T_SIZE sizeof(klist_node_t)
typedef struct klist_node_t {
	struct klist_node_t *prev;
	struct klist_node_t *next;
	void *value;
} klist_node_t;

/**
 * @brief head, tail and count of nodes
 * Caller allocates and frees, does not touch nodes.
 *
 */
#define KLIST_T_SIZE sizeof(klist_t)
typedef struct klist_t {
	klist_node_t *head;
	klist_node_t *tail;
	atomic_int count;
} klist_t;

/**
 * @brief return 0 for match, *NOT* ==; mimic strcmp returns
 *
 */
typedef int (*klist_comparison_f)(void *a, void *b);

/**
 * @brief Allocates and initializes new node and set value
 *
 * @param value p_value:...
 * @return struct klist_node_t*
 */
klist_node_t* klist_node_new_allocs(void *value);
/**
 * @brief frees the node only, value is untouched
 *
 * @param list_node p_list_node:...
 */
void klist_node_release_frees(klist_node_t *list_node);

/**
 * @brief Initializes given klist
 *
 * @return struct klist_t*
 */
void klist_init(klist_t *list);

/**
 * @brief Removes and frees all nodes, caller responsible for freeing (or not) list itself.  Values must be handled before call.
 *
 * @param list p_list:...
 */
void klist_release_frees(klist_t* list);

/**
 * @brief Finds node with same value address
 *
 * @param list p_list:...
 * @param ptr p_ptr:...
 * @return struct klist_node_t* | found node or NULL
 */
klist_node_t *klist_find_node_by_value_address(klist_t *list, void *ptr);

/**
 * @brief Cycles through all nodes using comparator
 *
 * @param list p_list:...
 * @param needle p_needle:...
 * @param comparator p_comparator:...
 * @return struct klist_node_t*
 */
klist_node_t *klist_find_node_by_comparison(klist_t*list, void* needle, klist_comparison_f comparator);

/**
 * @brief Unlinks a node
 *
 * @param list p_list:...
 * @param list_node p_list_node:...
 */
void klist_remove_node(klist_t *list, klist_node_t *list_node);


/**
 * @brief Moves a node after the given after klist_node_t.
 *
 * @param list p_list:...
 * @param list_node p_list_node:...
 * @param after p_after:...NULL moves to head.
 */
void klist_move_node(klist_t *list, klist_node_t *list_node, klist_node_t *after);


/**
 * @brief Inserts the value into new node before given node.  NULL appends it after tail.
 *
 * @param list p_list:...
 * @param before p_before:...
 * @param ptr p_ptr:...
 */
void klist_insert_node(klist_t *list, klist_node_t *before, klist_node_t *to_insert);

/**
 * @brief Inserts the value into new node before given node.  NULL appends it after tail.
 *
 * @param list p_list:...
 * @param before p_before:...
 * @param ptr p_ptr:...
 * @return klist_node_t* | Newly allocated node
 */
klist_node_t * klist_insert_value_allocs(klist_t * list, klist_node_t * after, void *ptr);


#endif
