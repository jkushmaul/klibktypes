#include <time.h>
#include <malloc.h>
#include <string.h>

#include <khashmap.h>

int main(int argc, char **argv) 
{
	const size_t MAX_ROUNDS = 100000;
	khashmap_t *map = khashmap_new_allocs(10000);
	
	size_t *keys = (size_t*)malloc(sizeof(size_t) * MAX_ROUNDS);
	size_t *values = (size_t*)malloc(sizeof(size_t) * MAX_ROUNDS);
	
	for (size_t i = 0; i < MAX_ROUNDS; i++) {
		keys[i] = i;
		values[i] = i;
	}
	struct timespec tstart = { 0, 0 }, tend = { 0, 0 };
	clock_gettime(CLOCK_MONOTONIC, &tstart);
	for (size_t i = 0; i < MAX_ROUNDS; i++) {
		khashmap_key_t key;
		key.data = (uint8_t*)&keys[i];
		key.len = sizeof(size_t);
		khashmap_put_allocs(map, &key, (void*)&values[i]);
	}
	clock_gettime(CLOCK_MONOTONIC, &tend);
	printf("%zu khashmap_put_allocs took about %.5f seconds\n", MAX_ROUNDS,
		   ((double)tend.tv_sec + 1.0e-9 * tend.tv_nsec) -
		   ((double)tstart.tv_sec + 1.0e-9 * tstart.tv_nsec));

	bzero(&tstart, sizeof(struct timespec));
	bzero(&tend, sizeof(struct timespec));
	clock_gettime(CLOCK_MONOTONIC, &tstart);
	for (size_t i = 0; i < MAX_ROUNDS; i++) {
		khashmap_key_t key;
		key.data = (uint8_t*)&keys[i];
		key.len = sizeof(size_t);
		size_t *val = khashmap_get(map, &key);
		if (val == NULL) {
			printf("Assertion failure with %zu, was NULL\n", i);	
		} else if (*val != i) {
			printf("Assertion failure with %zu, was %zu\n", i, *val);
		}
	}

	
	clock_gettime(CLOCK_MONOTONIC, &tend);
	printf("%zu khashmap_get took about %.5f seconds\n", MAX_ROUNDS,
		   ((double)tend.tv_sec + 1.0e-9 * tend.tv_nsec) -
		   ((double)tstart.tv_sec + 1.0e-9 * tstart.tv_nsec));
	
	printf("Looking for larger than 1 buckets:\n");
	for(int i = 0; i< map->table_size; i++) {
		if (map->buckets[i].values.count > 2){
			//printf("bucket[%d].values.count==%d\n", i, map->buckets[i].values.count );
		}
	}
	
	khashmap_release_frees(map);
	free(keys);
	free(values);
	return 0;
}
