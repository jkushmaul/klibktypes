#include "ksortedlist.h"

void ksortedlist_init(ksortedlist_t *list, klist_comparison_f comparator)
{
	klist_init((klist_t*)list);
	list->comparator = comparator;
}

klist_node_t *ksortedlist_add_allocs(ksortedlist_t *list, void *value)
{
	klist_node_t *insert_before = NULL;

	KLIST_FOR_EACH_NODE(node, &list->list) {
		int compare = list->comparator(node->value, value);

		if (compare >= 0) {
			insert_before = node;
			break;
		}
	}

	return klist_insert_value_allocs(&list->list, insert_before, value);
}
