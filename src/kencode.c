#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kencode.h>
#include <kstring.h>


int kencode_hex_value(char enc_char, uint8_t *dec_raw)
{
	int error = 0;
	char a = enc_char;
	uint8_t *ch = dec_raw;

	if (!kstring_char_in_range('0', a, '9')) {
		*ch = a - '0';
	} else if (!kstring_char_in_range('a', a, 'f')) {
		*ch = a - 'a' + 0x0a;
	} else if (!kstring_char_in_range('A', a, 'F')) {
		*ch = a - 'A' + 0x0a;
	} else{
		error = -1;
	}
	return error;
}

int kencode_hex_encode(uint8_t *src, size_t srclen, char *dst, size_t dstlen)
{
	int error = KENCODE_NO_ERROR;

	if (dstlen != srclen * 2) {
		return KENCODE_BAD_LENGTH;
	}
	for (size_t i = 0; i < srclen; i++) {
		sprintf((char*)dst + i * 2, "%.2x", src[i]);
	}
	return error;
}

int kencode_hex_decode(const char *src, size_t srclen, uint8_t *dst, size_t dstlen)
{
	int error = KENCODE_NO_ERROR;

	if ( (srclen) % 2 != 0 || (srclen) / 2 != dstlen) {
		error = KENCODE_BAD_LENGTH;
	} else{
		for (size_t i = 0; i < srclen; i += 2) {
			uint8_t a, b;
			if (kencode_hex_value(src[i], &a)) {
				error = KENCODE_BAD_CHAR;
			} else if (kencode_hex_value(src[i + 1], &b)) {
				error = KENCODE_BAD_CHAR;
			} else{
				uint8_t c = a << 4;
				c += b;
				dst[i / 2] = c;
			}
		}
	}
	return error;
}
