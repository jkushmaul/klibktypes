#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include <kfile.h>
#include <kstring.h>


int kfile_path_exists(const char *path)
{

	struct stat info;
	int exists = 1;

	//check dst does not exist
	if (lstat(path, &info) != 0) {
		if (errno == ENOENT) {
			//  doesn't exist
			exists = 0;
		} else{
			//general error handling
			exists = -1;
		}
	}
	return exists;
}

int kfile_move_noreplace(const char *src, const char *dst)
{
	int exists = kfile_path_exists(dst);
	int error = 0;

	if (exists) {
		error = -1;
	} else{
		//make sure it's parent does exist
		error = rename(src, dst);
	}
	return error;
}

int kfile_write_all(const char *fpath, const uint8_t *buffer, size_t len)
{
	int error = 0;
	FILE *f = fopen(fpath, "w");

	if (!f) {
		printf("Could not create meta file %s\n", fpath);
		error = -1;
	} else {
		size_t offset = 0;
		const size_t max_write = 4096;
		while (len > 0) {
			size_t len_to_write = len;
			if (len_to_write > max_write) {
				len_to_write = max_write;
			}
			size_t written = fwrite(buffer + offset, 1, len_to_write, f);
			if (written != len_to_write) {
				printf("Failed writing to %s\n", fpath);
				error = -1;
				break;
			}
			offset += written;
			len -= written;
		}
		fclose(f);
	}
	return error;
}

const char *kfile_next_dir(const char *path)
{
	if (path[0] == 0) {
		return NULL;
	}
	char *slash = strchr(path, '/');
	if (slash == NULL) {
		return NULL;
	} else{
		return slash + 1;
	}
}

char *kfile_top_dir_allocs(const char *path)
{
	if (path[0] == 0) {
		return NULL;
	}
	char *slash = strchr(path, '/');
	char *dirname = NULL;
	if (slash != NULL) {
		size_t len = 1 + slash - path;
		dirname = (char*)malloc(len);
		snprintf(dirname, len, "%s", path);
	} else{
		dirname = strdup(path);
	}

	return dirname;
}

int kfile_is_dir(const char *path)
{
	struct stat statbuf;

	if (stat(path, &statbuf) != 0) {
		return -1;
	}
	return S_ISDIR(statbuf.st_mode) == 0;
}

int kfile_dir_exists(const char *path)
{
	int error = 0;
	DIR *d = opendir(path);

	if (d == NULL) {
		error = -1;
	} else{
		closedir(d);
	}
	return error;
}

int kfile_mkdir(const char *path)
{
	int error = 0;

	if (kfile_dir_exists(path)) {
		error = mkdir(path, S_IRUSR | S_IWUSR | S_IXUSR);
	}
	return error;
}

int kfile_rmdir_recurse(char *path)
{
	int error = 0;

	DIR *d = opendir(path);

	if (!d) {
		return -1;
	}
	struct dirent *dir;
	while (!error && (dir = readdir( d )) != NULL) {
		if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")) {
			continue;
		}
		char *new_path = NULL;
		kstring_sprintf_allocs(&new_path, "%s/%s", path, dir->d_name);
		if (!kfile_is_dir(new_path)) {
			error = kfile_rmdir_recurse(new_path);
		} else{
			//delete files
			error = unlink(new_path);
		}
		free(new_path);
	}
	closedir(d);
	error = rmdir(path);
	return error;
}

int kfile_mkdir_recurse(char *base_path, char *path)
{
	if (strlen(path) == 0) {
		return 0;
	}
	int error = 0;
	//   /some/dir/here  or ./some/dir/here or some/dir/here
	char *top_dir = kfile_top_dir_allocs(path);
	if (top_dir != NULL) {
		char *new_path = NULL;
		if (base_path != NULL && strlen(base_path) != 0) {
			kstring_sprintf_allocs(&new_path, "%s/%s", base_path, top_dir);
		} else{
			new_path = strdup(top_dir);
		}

		error = kfile_mkdir(new_path);
		if (!error) {
			int slash = 1;
			if (strlen(path) == strlen(top_dir)) {
				slash = 0;
			}
			error = kfile_mkdir_recurse(new_path, path + strlen(top_dir) + slash);
		}
		free(new_path);
		free(top_dir);
	}
	return error;
}
