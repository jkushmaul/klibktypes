#include <khashmap.h>
#include <malloc.h>
#include <string.h>






khashmap_t *khashmap_new_allocs(size_t table_size)
{
	size_t size = sizeof(khashmap_t) + sizeof(khashmap_bucket_t) * table_size;
	khashmap_t *hashmap = (khashmap_t*)calloc(1, size);

	hashmap->table_size =  table_size;
	for (size_t i = 0; i < table_size; i++) {
		hashmap->buckets[i].index = i;
	}
	return hashmap;
}

uint32_t khashmap_key_hash(khashmap_key_t *key)
{
	uint32_t hash = 5381;
	uint32_t M = 33;

	for (int i = 0; i < key->len; i++) {
		hash = M * hash ^ key->data[i];
	}
	return hash;
}

int khashmap_key_compare(khashmap_key_t *left, khashmap_key_t *key)
{
	//smaller entries closer to front
	if (left->len == key->len) {
		return memcmp(left->data, key->data, key->len);
	} else {
		return left->len - key->len;
	}
}


void *khashmap_loop_compare(khashmap_entry_t* entry, uint32_t hash, khashmap_key_t *key)
{
	while ( (entry = (khashmap_entry_t*)entry->node.next) != NULL) {
		if (entry->key_hash == hash) {
			int compare = khashmap_key_compare(&entry->key, key);
			if (!compare) {
				return entry;
			} else if (compare > 0) {
				break;
			}
		} else if (entry->key_hash > hash) {
			break;
		}
	}
	return NULL;
}
khashmap_entry_t *khashmap_get_node(khashmap_t *hashmap, khashmap_bucket_t *bucket, uint32_t hash, khashmap_key_t *key)
{
	khashmap_entry_t *entry = (khashmap_entry_t*)bucket->values.head;

	if (entry) {
		if (entry->key_hash == hash && !khashmap_key_compare(&entry->key, key)) {
			return entry;
		}
	} else {
		return NULL;
	}
	if (entry->node.next == NULL) {
		return NULL;
	} else {
		return khashmap_loop_compare(entry, hash, key);
	}
}

void *khashmap_get(khashmap_t *hashmap, khashmap_key_t *key)
{
	uint32_t hash = khashmap_key_hash(key);
	size_t index = hash % hashmap->table_size;
	khashmap_bucket_t *bucket = &hashmap->buckets[index];
	khashmap_entry_t *entry = khashmap_get_node(hashmap, bucket, hash, key);

	if (entry != NULL) {
		return entry->value;
	}
	return NULL;
}

void *khashmap_put_allocs(khashmap_t *hashmap, khashmap_key_t *key, void *value)
{
	uint32_t hash = khashmap_key_hash(key);
	size_t index = hash % hashmap->table_size;
	khashmap_bucket_t *bucket = &hashmap->buckets[index];
	void *orig_value = NULL;

	klist_node_t *before = NULL;

	KLIST_FOR_EACH_NODE(node, &bucket->values) {
		khashmap_entry_t *left = (khashmap_entry_t*)node;

		before = (klist_node_t*)left;
		if (left->key_hash == hash) {
			int compare = khashmap_key_compare(&left->key, key);
			if (!compare) {
				orig_value = left->value;
				left->value = value;
				return orig_value;
			} else if (compare > 0) {
				break;
			}
		} else if (left->key_hash > hash) {
			break;
		}
		if (node == bucket->values.tail) {
			before = NULL;
		}
	}
	khashmap_entry_t *entry = calloc(1, sizeof(khashmap_entry_t));
	entry->node.value = (void*)entry;
	memcpy(&entry->key, key, sizeof(khashmap_key_t));
	entry->key_hash = hash;
	entry->value = value;

	klist_insert_node(&bucket->values, before, (klist_node_t*)entry);

	hashmap->total_entries++;

	return NULL;
}

void *khashmap_remove_bynode_frees(khashmap_t *hashmap, khashmap_bucket_t *bucket, khashmap_entry_t *entry)
{
	klist_remove_node(&bucket->values, (klist_node_t*)entry);
	hashmap->total_entries--;
	void *value = entry->value;
	free(entry);
	return value;
}
void *khashmap_remove_frees(khashmap_t *hashmap, khashmap_key_t *key)
{
	uint32_t hash = khashmap_key_hash(key);
	size_t index = hash % hashmap->table_size;
	khashmap_bucket_t *bucket = &hashmap->buckets[index];
	khashmap_entry_t *entry = khashmap_get_node(hashmap, bucket, hash, key);

	if (entry != NULL) {
		return khashmap_remove_bynode_frees(hashmap, bucket, entry);
	} else {
		return NULL;
	}

}
void khashmap_release_frees(khashmap_t *hashmap)
{
	for (size_t i = 0; i < hashmap->table_size; i++) {
		khashmap_bucket_t *bucket = &hashmap->buckets[i];

		while (bucket->values.head != NULL) {
			khashmap_entry_t *entry = (khashmap_entry_t*)bucket->values.head;
			khashmap_remove_bynode_frees(hashmap, bucket, entry);
		}
	}
	free(hashmap);
}



