#include <stdlib.h>
#include <strings.h>

#include "kindexedmap.h"


void kindexedmap_init_allocs(kindexedmap_t *imap, klist_comparison_f index_compare, size_t map_table_size)
{
	bzero(imap, sizeof(kindexedmap_t));
	imap->map = khashmap_new_allocs(map_table_size);
	ksortedlist_init(&imap->index, index_compare);
}

void kindexedmap_release_frees(kindexedmap_t *imap)
{
	if (imap->map != NULL) {
		khashmap_release_frees(imap->map);
		imap->map = NULL;
	}
	klist_release_frees((klist_t*)&imap->index);
}

void *kindexedmap_put_allocs(kindexedmap_t *imap, khashmap_key_t *key, void *value)
{
	void *orig_value = kindexedmap_remove_frees(imap, key);
	klist_node_t *new_node = ksortedlist_add_allocs(&imap->index, value);

	khashmap_put_allocs(imap->map, key, new_node);
	return orig_value;
}


void *kindexedmap_get(kindexedmap_t *imap, khashmap_key_t *key)
{
	klist_node_t *node = khashmap_get(imap->map, key);

	if (node) {
		return node->value;
	} else {
		return NULL;
	}
}


void *kindexedmap_remove_node_frees(kindexedmap_t *imap, klist_node_t *node)
{
	klist_remove_node((klist_t*)&imap->index, node);
	void *value = node->value;
	free(node);
	return value;
}
void *kindexedmap_remove_frees(kindexedmap_t *imap, khashmap_key_t *key)
{
	klist_node_t *node = khashmap_remove_frees(imap->map, key);

	if (node) {
		return kindexedmap_remove_node_frees(imap, node);
	} else {
		return NULL;
	}
}
