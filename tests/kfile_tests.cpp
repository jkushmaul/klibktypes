
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;

extern "C" {
#include <stdio.h>
#include "kfile.h"
#include <dirent.h>
#include <sys/stat.h>

}


int kfile_rmdir_recurse(char *path);
int kfile_is_dir(const char *path);

TEST(kfile, kfile_is_dir){

	ASSERT_EQ(-1, kfile_is_dir("pleasedontcreatethisdir"));

	kfile_mkdir("some");
	ASSERT_EQ(0, kfile_dir_exists("some"));
	ASSERT_EQ(0, kfile_is_dir("some"));

	FILE *f = fopen("some/file", "w");
	ASSERT_NE((FILE*)NULL, f);
	fclose(f);
	ASSERT_EQ(1, kfile_is_dir("some/file"));
	unlink("some/file");
	rmdir("some");
}

TEST(kfile, kfile_rmdir_recurse){
	kfile_mkdir_recurse(NULL, (char*)"some/other/dir");
	FILE *f = fopen("some/other/dir/file", "w");
	ASSERT_NE((FILE*)NULL, f);
	fclose(f);
	kfile_rmdir_recurse((char*)"some");
	ASSERT_NE(0, kfile_dir_exists("some"));
}


TEST(kfile, kfile_mkdir_recurse){
	const char *path = "some/other/dir";

	rmdir(path);
	int error = kfile_mkdir_recurse((char*)"", (char*)path);
	ASSERT_EQ(0, error);
	error = kfile_mkdir_recurse((char*)"", (char*)path);
	ASSERT_EQ(0, error);

	DIR *d = opendir(path);
	ASSERT_NE((DIR*)NULL, d);
	closedir(d);
	rmdir(path);
}

TEST(kfile, kpass_mkdir){
	const char *path = "some";

	rmdir(path);
	int error = kfile_mkdir(path);
	ASSERT_EQ(0, error);
	error = kfile_mkdir(path);
	ASSERT_EQ(0, error);
	rmdir(path);
}

TEST(kfile, kpass_dir_exists){
	ASSERT_EQ(0, kfile_dir_exists((char*)"."));
	ASSERT_EQ(-1, kfile_dir_exists((char*)"pleasedontcreatethisdir"));
}

TEST(kfile, kfile_next_dir){
	const char *path = "some/dir/path";
	const char *next_dir = kfile_next_dir(path);

	ASSERT_EQ(path + 5, next_dir);
}

TEST(kfile, kpass_top_dir_allocs){
	const char *path = "some/dir/path";
	char *topdir = kfile_top_dir_allocs((char*)path);

	ASSERT_EQ(0, strcmp(topdir, "some"));
	free(topdir);

	const char *path2 = "/some/dir/path";
	topdir = kfile_top_dir_allocs((char*)path2);
	ASSERT_EQ(0, strlen(topdir));
	free(topdir);
}


TEST(kfile, kpass_file_write_all__writes){
	uint8_t buffer[4096];
	uint8_t c = 0;

	for (size_t i = 0; i < sizeof(buffer); i++) {
		buffer[i] = c;
		if (c == 0xff) {
			c = 0;
		} else{
			c++;
		}
	}
	const char *fpath = "kpass_file_write_all.test.data";
	int error = kfile_write_all(fpath, buffer, sizeof(buffer));
	ASSERT_EQ(0, error);

	FILE *f = fopen(fpath, "rb");
	//should create
	ASSERT_NE((FILE*)NULL, f);
	//should write all data
	int n = fread(buffer, sizeof(buffer), 1, f);
	ASSERT_EQ(1, n);

	c = 0;
	for (size_t i = 0; i < sizeof(buffer); i++) {
		ASSERT_EQ(buffer[i], c);
		if (c == 0xff) {
			c = 0;
		} else{
			c++;
		}
	}
	fclose(f);
	unlink(fpath);
}

TEST(kfile, kfile_move_noreplace){
	int error;
	char *src;

	const char *dest_not_exist = "some/file_new";

	src = (char*)"some/file";

	kfile_mkdir("some");
	FILE *f = fopen(src, "w");
	fclose(f);

	error = kfile_move_noreplace(src, dest_not_exist);
	ASSERT_EQ(0, error);

	error = kfile_move_noreplace(src, dest_not_exist);
	ASSERT_EQ(-1, error);
}

TEST(kfile, kpass_file_write_all__fails){
	const char *fpath = "pleasedontcreatethisdir/kpass_file_write_all.test.data";
	uint8_t buffer[256];
	int error = kfile_write_all(fpath, buffer, sizeof(buffer));

	ASSERT_EQ(-1, error);
}
