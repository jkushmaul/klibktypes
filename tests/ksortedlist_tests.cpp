extern "C" {
    #include <string.h>
}
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;
#include <atomic>
using namespace std;

extern "C" {
    #include <ksortedlist.h>
}

int compare_ints_asc(void *left, void *right)
{
	int ileft = *(int*)left;
	int iright = *(int*)right;

	return ileft - iright;
}
TEST(ksortedlist, ksortedlist_add_allocs) {
	ksortedlist_t list = { };

	ksortedlist_init(&list, compare_ints_asc);

	int one = 1;
	int two = 2;
	int three = 3;
	ksortedlist_add_allocs(&list, &three);
	ksortedlist_add_allocs(&list, &one);
	ksortedlist_add_allocs(&list, &two);

	klist_node_t *node;
	node = list.list.head;

	ASSERT_EQ(&one, node->value);
	node = node->next;
	ASSERT_EQ(&two, node->value);
	node = node->next;
	ASSERT_EQ(&three, node->value);
	ASSERT_EQ((klist_node_t*)NULL, node->next);
	klist_release_frees((klist_t*)&list);
}
