#include <gtest/gtest.h>
#include <atomic>
using namespace std;
extern "C" {
#include "klist.h"
}



int compare_int_by_value(void* a, void*b)
{
	int i_a = *(int*)a;
	int i_b = *(int*)b;

	if (i_a == i_b) {
		return 0;
	} else if (i_a > i_b) {
		return 1;
	} else{
		return -1;
	}
}

TEST(list, create){
	klist_t list;

	klist_init(&list);

	ASSERT_EQ(NULL, list.head);
	klist_release_frees(&list);

	int value = 3;
	klist_node_t *node = klist_node_new_allocs(&value);
	ASSERT_TRUE(node != NULL);
	klist_node_release_frees(node);
}
TEST(list, insert_value_null){
	klist_t list;

	klist_init(&list);
	int i = 1, j = 2;

	klist_insert_value_allocs(&list, NULL, &i);
	klist_insert_value_allocs(&list, NULL, &j);//moves it to tail if NULL
	ASSERT_EQ(list.count, 2);
	ASSERT_EQ(list.head->value, (&i));
	ASSERT_EQ(list.tail->value, (&j));

	klist_release_frees(&list);
}
TEST(list, insert_value_non_null){
	klist_t list;

	klist_init(&list);

	int i = 1, j = 2;

	klist_node_t *node = klist_insert_value_allocs(&list, 0, &i);
	klist_insert_value_allocs(&list, node, &j);//inserts it before node
	ASSERT_EQ(list.count, 2);
	ASSERT_EQ(list.head->value, (&j));
	ASSERT_EQ(list.tail->value, (&i));

	klist_release_frees(&list);
}

TEST(list, remove){
	klist_t list;

	klist_init(&list);

	uint32_t array[5];
	klist_node_t *nodes[5];

	for (int i = 0; i < 5; i++) {
		array[i] = i;
		nodes[i] = klist_insert_value_allocs(&list, NULL, &array[i]);
	}
	ASSERT_EQ(5, list.count);
	ASSERT_EQ(nodes[0], list.head);
	ASSERT_EQ(nodes[4], list.tail);

	//remove head node
	klist_node_t *node;
	node = list.head;
	klist_remove_node(&list, node);
	ASSERT_EQ(4, list.count);
	ASSERT_EQ(nodes[1], list.head);
	ASSERT_EQ((klist_node_t*)0, list.head->prev);
	klist_node_release_frees(node);

	//remove tail node
	node = list.tail;
	klist_remove_node(&list, node);
	ASSERT_EQ(3, list.count);
	ASSERT_EQ(nodes[3], list.tail);
	ASSERT_EQ((klist_node_t*)0, list.tail->next);
	klist_node_release_frees(node);

	//remove middle node
	klist_remove_node(&list, nodes[2]);
	ASSERT_EQ(2, list.count);
	ASSERT_EQ(nodes[3], list.head->next);
	ASSERT_EQ(nodes[1], list.tail->prev);
	klist_node_release_frees(nodes[2]);

	klist_release_frees(&list);
}


TEST(list, list_find_node_by_comparison){
	klist_t list;

	klist_init(&list);
	uint32_t array[5];
	klist_node_t *nodes[5];

	for (int i = 0; i < 5; i++) {
		array[i] = i;
		nodes[i] = klist_insert_value_allocs(&list, NULL, &array[i]);
	}
	ASSERT_EQ(5, list.count);



	klist_node_t *node = 0;
	node = klist_find_node_by_comparison(&list, list.head->value, compare_int_by_value);
	ASSERT_EQ(nodes[0], node);

	node = klist_find_node_by_comparison(&list, list.tail->value, compare_int_by_value);
	ASSERT_EQ(nodes[4], node);

	node = klist_find_node_by_comparison(&list, nodes[3]->value, compare_int_by_value);
	ASSERT_EQ(nodes[3], node);

	int doesnotexist = 42;
	node = klist_find_node_by_comparison(&list, &doesnotexist, compare_int_by_value);
	ASSERT_EQ((klist_node_t*)0, node);

	klist_t empty_list = { 0 };
	ASSERT_EQ(0, (uint64_t)klist_find_node_by_comparison(&empty_list, nodes[3]->value, compare_int_by_value));

	klist_release_frees(&list);
}


TEST(list, list_find_node_by_value_address){
	klist_t list;

	klist_init(&list);
	uint32_t array[5];
	klist_node_t *nodes[5];

	for (int i = 0; i < 5; i++) {
		array[i] = i;
		nodes[i] = klist_insert_value_allocs(&list, NULL, &array[i]);
	}
	ASSERT_EQ(5, list.count);

	klist_node_t *node = 0;
	node = klist_find_node_by_value_address(&list, list.head->value);
	ASSERT_EQ(nodes[0], node);

	node = klist_find_node_by_value_address(&list, list.tail->value);
	ASSERT_EQ(nodes[4], node);

	node = klist_find_node_by_value_address(&list, nodes[3]->value);
	ASSERT_EQ(nodes[3], node);

	int doesnotexist = 1;
	node = klist_find_node_by_value_address(&list, &doesnotexist);
	ASSERT_EQ((klist_node_t*)0, node);




	klist_release_frees(&list);
}
