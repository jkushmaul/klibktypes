#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <atomic>

using namespace testing;
using namespace std;


extern "C" {
    #include <stdio.h>
    #include <khashmap.h>
}


TEST(khashmap, khashmap_for_each_value) {
	const size_t MAX_ROUNDS = 100;
	khashmap_t *map = khashmap_new_allocs(1000);

	size_t *keys = (size_t*)malloc(sizeof(size_t) * MAX_ROUNDS);
	size_t *values = (size_t*)malloc(sizeof(size_t) * MAX_ROUNDS);

	for (size_t i = 0; i < MAX_ROUNDS; i++) {
		keys[i] = i;
		values[i] = MAX_ROUNDS - i - 1;
	}

	khashmap_entry_t *entry = NULL;
	KHASHMAP_FOR_EACH_VALUE(map, entry) {
		size_t key = *entry->key.data;
		size_t value = *((size_t*)entry->value);

		ASSERT_NE(0, key);
		ASSERT_NE(0, value);
	}

	khashmap_release_frees(map);
	free(keys);
	free(values);
}

TEST(khashmap, khashmap_remove_frees) {
	khashmap_t *hashmap = khashmap_new_allocs(1000);
	khashmap_key_t key = { 0 };
	char *result;
	char *value;

	value = NULL;
	key.data = (uint8_t*)"test";
	key.len = 4;
	result = (char*)khashmap_get(hashmap, &key);

	ASSERT_EQ(result, value);


	value = (char*)"testvalue";
	result = (char*)khashmap_put_allocs(hashmap, &key, value);
	ASSERT_EQ(result, (char*)NULL);

	result = (char*)khashmap_get(hashmap, &key);
	ASSERT_EQ(result, value);

	khashmap_remove_frees(hashmap, &key);
	result = (char*)khashmap_get(hashmap, &key);
	value = NULL;
	ASSERT_EQ(result, value);

	khashmap_release_frees(hashmap);
}



TEST(khashmap, khashmap_new){
	khashmap_t *null = NULL;
	khashmap_t *hashmap = khashmap_new_allocs(1000);

	ASSERT_NE(null, hashmap);
	ASSERT_EQ(1000, hashmap->table_size);
	khashmap_release_frees(hashmap);
}

TEST(khashmap, khashmap_get_put_once){
	khashmap_t *hashmap = khashmap_new_allocs(1000);
	khashmap_key_t key = { 0 };
	char *result;
	char *value;

	value = NULL;
	key.data = (uint8_t*)"test";
	key.len = 4;
	result = (char*)khashmap_get(hashmap, &key);

	ASSERT_EQ(result, value);


	value = (char*)"testvalue";
	result = (char*)khashmap_put_allocs(hashmap, &key, value);
	ASSERT_EQ(result, (char*)NULL);

	result = (char*)khashmap_get(hashmap, &key);
	ASSERT_EQ(result, value);

	khashmap_release_frees(hashmap);

}

TEST(khashmap, khashmap_get_put_twice){
	khashmap_t *hashmap = khashmap_new_allocs(1000);
	khashmap_key_t key = { 0 };
	char *result;
	char *value;

	value = NULL;
	key.data = (uint8_t*)"test";
	key.len = 4;
	result = (char*)khashmap_get(hashmap, &key);

	ASSERT_EQ(result, value);


	value = (char*)"testvalue";
	result = (char*)khashmap_put_allocs(hashmap, &key, value);
	ASSERT_EQ(result, (char*)NULL);
	result = (char*)khashmap_get(hashmap, &key);
	ASSERT_EQ(result, value);

	char *value2 = (char*)"test2value";
	result = (char*)khashmap_put_allocs(hashmap, &key, value2);
	ASSERT_EQ(result, value);
	result = (char*)khashmap_get(hashmap, &key);
	ASSERT_EQ(result, value2);

	khashmap_release_frees(hashmap);
}

TEST(khashmap, khashmap_key_compare_equals) {
	khashmap_key_t left = { 0 }, right = { 0 };

	left.data = (uint8_t*)"a";
	left.len = 1;
	right.data = (uint8_t*)"a";
	right.len = 1;

	ASSERT_EQ(0, khashmap_key_compare(&left, &right));
}

TEST(khashmap, khashmap_key_compare_less_length_negative) {
	khashmap_key_t left = { 0 }, right = { 0 };

	left.data = (uint8_t*)"a";
	left.len = 1;
	right.data = (uint8_t*)"aa";
	right.len = 2;

	ASSERT_EQ(-1, khashmap_key_compare(&left, &right));

	right.data = (uint8_t*)"aaa";
	right.len = 3;
	ASSERT_EQ(-2, khashmap_key_compare(&left, &right));
}


TEST(khashmap, khashmap_key_compare_greater_length_positive) {
	khashmap_key_t left = { 0 }, right = { 0 };

	left.data = (uint8_t*)"a";
	left.len = 1;
	right.data = (uint8_t*)"aa";
	right.len = 2;

	ASSERT_EQ(1, khashmap_key_compare(&right, &left));

	right.data = (uint8_t*)"aaa";
	right.len = 3;
	ASSERT_EQ(2, khashmap_key_compare(&right, &left));
}


TEST(khashmap, khashmap_key_compare_same_key_left_less) {
	khashmap_key_t left = { 0 }, right = { 0 };

	left.data = (uint8_t*)"a";
	left.len = 1;
	right.data = (uint8_t*)"b";
	right.len = 1;

	ASSERT_EQ(-1, khashmap_key_compare(&left, &right));
}

TEST(khashmap, khashmap_key_compare_same_key_left_greater) {
	khashmap_key_t left = { 0 }, right = { 0 };

	left.data = (uint8_t*)"b";
	left.len = 1;
	right.data = (uint8_t*)"a";
	right.len = 1;

	ASSERT_EQ(1, khashmap_key_compare(&left, &right));
}


TEST(khashmap, khashmap_get_put_513){
	khashmap_t *hashmap = khashmap_new_allocs(10000);
	size_t testkey = 513;
	khashmap_key_t statickey = { 0 };

	statickey.data = (uint8_t*)&testkey;
	statickey.len = sizeof(size_t);
	uint32_t hash = khashmap_key_hash(&statickey);
	size_t index = hash % hashmap->table_size;
	khashmap_bucket_t *bucket = &hashmap->buckets[index];

	size_t keyv[] = { 513, 25344, 625, 61196, 52239, 41218, 36365, 16387, 9478, 9072, 17527, 57714, 49267, 25974, 36477, 44924 };

	for (size_t i = 0; i < sizeof(keyv) / sizeof(size_t); i++) {
		khashmap_key_t key = { 0 };
		key.data = (uint8_t*)&keyv[i];
		key.len = sizeof(size_t);
		size_t *value = &keyv[i];

		size_t *result = (size_t*)khashmap_put_allocs(hashmap, &key, value);
		ASSERT_EQ(result, (size_t*)NULL);
		uint32_t myhash = khashmap_key_hash(&key);
		size_t myindex = myhash % hashmap->table_size;
		ASSERT_EQ(index, myindex) << "Index not equal for item " << keyv[i];
	}


	ASSERT_EQ(sizeof(keyv) / sizeof(size_t), bucket->values.count);

	for (size_t i = 0; i < sizeof(keyv) / sizeof(size_t); i++) {
		khashmap_key_t key = { 0 };
		key.data = (uint8_t*)&keyv[i];
		key.len = sizeof(size_t);
		size_t *value = &keyv[i];

		size_t *result = (size_t*)khashmap_get(hashmap, &key);

		if (result != NULL) {
			ASSERT_EQ(result, value) << "Expected " << *value << " but got " << *result;
		} else {
			ASSERT_EQ(result, value) << "Expected " << *value << " but got NULL";
		}

	}

	khashmap_release_frees(hashmap);

}


TEST(khashmap, khashmap_get_put_once_140){
	khashmap_t *hashmap = khashmap_new_allocs(10000);

	size_t statickey = 140;
	khashmap_key_t key = { 0 };

	key.data = (uint8_t*)&statickey;
	key.len = sizeof(size_t);
	size_t *value = &statickey;

	size_t *result = (size_t*)khashmap_put_allocs(hashmap, &key, value);
	ASSERT_EQ(result, (size_t*)NULL);

	result = (size_t*)khashmap_get(hashmap, &key);
	if (result != NULL) {
		ASSERT_EQ(result, value) << "Expected " << *value << " but got " << *result;
	} else {
		ASSERT_EQ(result, value) << "Expected " << *value << " but got NULL";
	}
	khashmap_release_frees(hashmap);

}




TEST(khashmap, khashmap_get_put_once_100){
	khashmap_t *hashmap = khashmap_new_allocs(10000);

	size_t statickey = 100;
	khashmap_key_t key = { 0 };

	key.data = (uint8_t*)&statickey;
	key.len = sizeof(size_t);
	size_t *value = &statickey;

	size_t *result = (size_t*)khashmap_put_allocs(hashmap, &key, value);
	ASSERT_EQ(result, (size_t*)NULL);

	result = (size_t*)khashmap_get(hashmap, &key);
	if (result != NULL) {
		ASSERT_EQ(result, value) << "Expected " << *value << " but got " << *result;
	} else {
		ASSERT_EQ(result, value) << "Expected " << *value << " but got NULL";
	}
	khashmap_release_frees(hashmap);

}
