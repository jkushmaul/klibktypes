klibktypes [![pipeline status](https://gitlab.com/jkushmaul/klibktypes/badges/master/pipeline.svg)](https://gitlab.com/jkushmaul/klibktypes/commits/master) [![coverage report](https://gitlab.com/jkushmaul/klibktypes/badges/master/coverage.svg)](https://gitlab.com/jkushmaul/klibktypes/commits/master) 

In support of other projects I had fun making these vs just using one of the many other fine
projects written.

* Source: https://gitlab.com/jkushmaul/klibktypes
* Docs: https://jkushmaul.gitlab.io/klibktypes/index.html
* Coverage: https://jkushmaul.gitlab.io/klibktypes/coverage/index.html
* Releases: https://gitlab.com/api/v4/projects/3618443/releases
** Ask gitlab to implement a UI for it.


TODO:
    * Not a fan of having to free klist_node_t outside of remove_node (Value - yes, node not.)  Make it more like khashmap.
    * Not a fan of khashmap_key_t not containing the actual key itself, but rather a ptr to it.  Hasn't quite caused me problems yet 
       But I could see it causing problems int he future.
	* Not quite a fan of passing khashmap_key_t to functions - instead uint8_t* + size_t ? Not sure.
    * Throwing around macros for "generics"
        Instead of ktypes_list_t; I'd have KTYPES_LIST(list_type, node_type, node_value_type) symbol; e.g. KTYPES_LIST(char *)
        I'm not sure how to do it yet, but one could have a macro that makes a new typedef
        DEFINE_KTYPES_LIST(char_list, char_list_node, char *);
        char_list my_list;
        memset(&my_list, 0, sizeof(char_list));
        
        The macro, used in a header, would result in this:
        typedef struct char_list {
            char_list_t *head; char_list_t *tail; int count;
        } char_list;
        typedef struct char_list_node {
            char_list_node *prev; char_list_node *next;
            char *value;
        } char_list_node;
        And I'd use it with existing klist functions.
        and then type errors
